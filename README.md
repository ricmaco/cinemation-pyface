## cinemation-pyface

### Hardware Prerequisites:
  * PiFace: http://www.piface.org.uk/

### Software Prerequisites:

```bash
$ sudo apt-get install python-tornado 
$ sudo apt-get install python-simplejson
```

### PiFace server:

```bash
$ git clone https://gitlab.com/ricmaco/cinemation-pyface.git
$ cd cinemation-pyface
$ python server.py
```

### Browser: 
  * http//<your_raspberrypi_ip]:8888

### References:
  * http://www.piface.org.uk/guides/Install_PiFace_Software/
  * https://piface.github.io/pifacedigitalio/
