#!/usr/bin/env python

PORT = 8888
# SETTINGS_FILE = "/home/pi/cinemation_webrele.ini"
SETTINGS_FILE = "settings.ini"

# Python WebSocket Server for Raspberry Pi / PiFace
# by David Art [aka] adcomp <david.madbox@gmail.com>
# modified by Rosario Crispo <info@datasoftweb.com>
# modified by Riccardo Macoratti <r.macoratti@gmx.co.uk>

import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web

import datetime
import json
import sys
import os
import socket
import fcntl
import struct
import logging
import ConfigParser

import pifacedigitalio as pfio
pfio.init()
pifacedigital = pfio.PiFaceDigital()

logger = logging.getLogger('I/O Web Server')
hdlr = logging.FileHandler('/var/log/cinemation/cinemation.log')
formatter = logging.Formatter("%(asctime)s: %(message)s", "%Y-%m-%d %H:%M:%S")
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.DEBUG)

def read_settings():
  global config
  config = ConfigParser.ConfigParser()
  config.read(SETTINGS_FILE)

def write_settings():
  global config
  with open(SETTINGS_FILE, 'w') as configfile:
    config.write(configfile)

def get_names_json():
  ret = {
    'inputs': {},
    'outputs': {}
  }

  for c in range(8):
    current = 'input{}'.format(c+1)
    ret['inputs'][current] = config.get('inputs', current)
    current = 'output{}'.format(c+1)
    ret['outputs'][current] = config.get('outputs', current)

  return ret

def set_names_json(data):
  global config
  
  for c in range(8):
    current = 'input{}'.format(c+1)
    config.set('inputs', current, data['inputs'][current]) 
    current = 'output{}'.format(c+1)
    config.set('outputs', current, data['outputs'][current])

def get_info_json():
  return {
    'title': config.get('info', 'title'),
    'subtitle': config.get('info', 'subtitle'),
    'pagetitle': config.get('info', 'pagetitle')
  }

def set_info_json(data):
  global config
  
  config.set('info', 'title', data['title']) 
  config.set('info', 'subtitle', data['subtitle']) 
  config.set('info', 'pagetitle', data['pagetitle']) 

def get_ip_address(ifname):
  global ip
  ip = '127.0.0.1'
  s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  ip = socket.inet_ntoa(fcntl.ioctl(
      s.fileno(),
      0x8915,  # SIOCGIFADDR
      struct.pack('256s', ifname[:15])
  )[20:24])

class IndexHandler(tornado.web.RequestHandler):
    
    @tornado.web.asynchronous
    def get(self):
      self.render("index.html",
        ip = ip
      )

class SettingsHandler(tornado.web.RequestHandler):
    
    @tornado.web.asynchronous
    def get(self):
      self.render("settings.html",
        ip = ip
      )

class WebSocketHandler(tornado.websocket.WebSocketHandler):

  clients = []
  last_data = None

  def open(self):
    self.connected = True
    self.clients.append(self)
    self.write_message(json.dumps({
      "names": get_names_json()
    }))
    self.write_message(json.dumps({
      'info': get_info_json()
    }))
    self.__timeout_loop()

  # """ Tornado 4.0 introduced an, on by default, same origin check.
  # This checks that the origin header set by the browser is the same as the host header """
  def check_origin(self, origin):
    return True

  def on_message(self, message):
    # it should receive a JSON like { "<identifier>": <object> }
    try:
      # try to parse JSON and discover type of message
      data = json.loads(message)
      if "status" in data:
        # body is the PIN number you want to toggle
        pin = int(data["status"])
        # read output state
        r_output = '{0:08b}'.format(pifacedigital.output_port.value)
        # toggle output
        pin_state = int(r_output[7-pin]);
        pfio.digital_write(pin, not pin_state)
        # FIXME! I need to check with the loop so I don't send twice ?
        self.__timeout_loop()
      elif "names" in data:
        # dict of names for input/output relays
        names = data["names"]
        # first compose config config
        set_names_json(names)
        # then write it
        write_settings()
        # re-read config
        names = get_names_json()
        logger.info("Written names: {}".format(json.dumps(names)))
        # finally send it to client, appending identifier
        self.write_message(json.dumps({
          "names": names
        }))
      elif "info" in data:
        # extract real data
        info = data["info"]
        # set data into config
        set_info_json(info)
        # write config
        write_settings()
        # re-read config
        info = get_info_json()
        logger.info("Written info: {}".format(json.dumps(info)))
        # send updated to client
        self.write_message(json.dumps({
          'info': get_info_json()
        }))
    except:
      logger.error("Message not valid: {}", message)

  def on_close(self):
    self.connected = False
    self.clients.remove(self)

  def __timeout_loop(self):
    # read PiFace input/output state
    r_input = '{0:08b}'.format(pifacedigital.input_port.value)
    r_output = '{0:08b}'.format(pifacedigital.output_port.value)

    # obj -> javascript
    data = {"in": [], "out": []}

    for i in range(8):
      data['in'].append(r_input[7-i])
      data['out'].append(r_output[7-i])

    # only send message if input/output changed
    if data != self.last_data:
      for client in self.clients:
        client.write_message(json.dumps({
          "status": data
        }))
        logger.info("I/O Status changed: {}".format(data))
    self.last_data = data

    # here come the magic part .. loop
    # FIXME! this is going pretty bad if too many clients I think
    # no other way to do this ?
    if self.connected:
      tornado.ioloop.IOLoop.instance().add_timeout(datetime.timedelta(seconds=.5), self.__timeout_loop)

application = tornado.web.Application([
  (r'/', IndexHandler),
  (r'/settings', SettingsHandler),
  (r'/ws', WebSocketHandler)
])

if __name__ == "__main__":
  http_server = tornado.httpserver.HTTPServer(application)
  http_server.listen(PORT)
  
  print("Cinemation I/O started on port {}".format(PORT))
  logger.info("Cinemation I/O started on port {}".format(PORT))
  
  # read configuration
  read_settings()
  logger.info("Read configuration: {}".format(json.dumps(get_names_json())))

  # get ip address
  get_ip_address("eth0")
  logger.info("Web Relay IP: {}".format(ip))
  
  try:
    tornado.ioloop.IOLoop.instance().start()
  except KeyboardInterrupt:
    print('\nExit')
    sys.exit(0)